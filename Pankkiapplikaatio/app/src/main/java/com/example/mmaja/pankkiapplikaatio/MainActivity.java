package com.example.mmaja.pankkiapplikaatio;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private FileIO io;
    private EditText userID;
    private EditText password;
    private TextView notification;
    private Bank bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userID = findViewById(R.id.etUserID);
        password = findViewById(R.id.etPassword);
        notification = findViewById(R.id.twNotification);
        io = new FileIO();
        bank = null;


        // Download bank information from file. If bank information is not
        // found, then a new bank is created.
        bank = io.loadBank(MainActivity.this);
        if (bank == null) {
            bank = Bank.getInstance();
            io.saveBank(bank, MainActivity.this);
        }
    }

    public void logIn (View v) {
        String inputUsername = userID.getText().toString();
        String inputPassword = password.getText().toString();
        boolean userNotFound = true;

        for(User u : bank.getUserList()) {
            if (u.getUserID().equals(inputUsername) && u.getPassword().equals(inputPassword)) {
                Intent intent = new Intent(MainActivity.this, MainMenu.class);
                intent.putExtra("userid", u.getUserID());
                startActivity(intent);
                userNotFound = false;
                finish();
                break;
            }

        }
        if (userNotFound == true)
            notification.setText("Virheellinen käyttäjätunnus tai salasana");
    }

    public void createUser (View v) {
        Intent intent = new Intent(MainActivity.this, CreateUser.class);
        startActivity(intent);
        finish();
    }
}



