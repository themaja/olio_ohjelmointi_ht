package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Tab1AccountsFragment extends Fragment implements AccountRecyclerViewAdapter.ItemClickListener {
    private static final String TAG = "Tab1AccountsFragment";

    AccountRecyclerViewAdapter adapter;

    private Bank bank;
    private String userid;
    private User user;
    private FileIO io;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab1_accounts_fragment, container, false);

        io = new FileIO();
        bank = io.loadBank(this.getActivity());
        userid = getArguments().getString("userid");
        user = bank.getUserByID(userid);

        // Populate the RecyclerView with information of each account
        ArrayList<String> accounts = new ArrayList<>();
        for (Account a : user.getAccountList()) {
            String type;
            if (a.getSavingsAccount() == true) {
                type = "säästötili";
            } else {
                type = "käyttötili";
            }
            String accountInfo = String.format("%s\t\t%s\n%.2f €", a.getAccountID(), type, a.getBalance());
            accounts.add(accountInfo);
        }

        // set up the RecyclerView
        RecyclerView recyclerView = rootView.findViewById(R.id.rvAccounts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AccountRecyclerViewAdapter(getActivity(), accounts);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), EditAccount.class);
        intent.putExtra("userid", userid);
        intent.putExtra("accIndex", position);
        startActivity(intent);
    }
}
