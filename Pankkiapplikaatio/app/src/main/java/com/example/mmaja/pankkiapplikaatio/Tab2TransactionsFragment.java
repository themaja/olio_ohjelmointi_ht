package com.example.mmaja.pankkiapplikaatio;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Tab2TransactionsFragment extends Fragment {
    private static final String TAG = "Tab2TransactionsFragment";

    TransactionRecyclerViewAdapter adapter;

    private Bank bank;
    private String userid;
    private User user;
    private Account account;
    private FileIO io;
    private ArrayList<String> transactionsData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab2_transactions_fragment, container, false);

        io = new FileIO();
        bank = io.loadBank(this.getActivity());
        userid = getArguments().getString("userid");
        user = bank.getUserByID(userid);

        // Populate the spinner with accounts
        List<String> spinnerArray =  new ArrayList<String>();
        for (Account a : user.getAccountList()) {
            String type;
            if (a.getSavingsAccount()) {
                type = "säästötili";
            } else {
                type = "käyttötili";
            }
            String accountInfo = String.format("%s %s %.2f €", a.getAccountID(), type, a.getBalance());
            spinnerArray.add(accountInfo);
        }

        // Set up the spinner
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sAccounts = rootView.findViewById(R.id.spAccounts);
        sAccounts.setAdapter(arrayAdapter);

        // set up the RecyclerView
        RecyclerView recyclerView = rootView.findViewById(R.id.rvTransactions);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new TransactionRecyclerViewAdapter(getActivity(), transactionsData);
        recyclerView.setAdapter(adapter);

        // set up an onItemsSelectedListener for the spinner to change transactions shown
        // in the recyclerview according to the selected account
        sAccounts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                account = user.getAccountList().get(position);
                String accountID = account.getAccountID();
                String transactionInfo = "";
                String date = "";
                // Populate the RecyclerView with information of each transaction
                ArrayList<String> newTransactionsData = new ArrayList<>();
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                for (Transaction t : bank.getTransactionList()) {
                    if (t.getFromAccountID().equals(accountID)) {
                        date = df.format(t.getDate());
                        if (t.getType() == Type.withdraw) {
                            transactionInfo = String.format("Nosto -%.2f €\n%s", t.getAmount(), date);
                        } else if (t.getType() == Type.transfer) {
                            transactionInfo = String.format("Maksettu -%.2f € tilille %s\n%s",t.getAmount(), t.getToAccountID(), date);
                        } else if (t.getType() == Type.cardPayment) {
                            transactionInfo = String.format("Korttimaksu -%.2f €\n%s",t.getAmount(), date);
                        }
                        newTransactionsData.add(transactionInfo);
                    } else if (t.getToAccountID().equals(accountID)) {
                        date = df.format(t.getDate());
                        if (t.getType() == Type.deposit) {
                            transactionInfo = String.format("Talletus %.2f €\n%s", t.getAmount(), date);
                        } else if (t.getType() == Type.transfer) {
                            transactionInfo = String.format("Vastaanotettu %.2f € tililtä %s\n%s", t.getAmount(), t.getFromAccountID(), date);
                        }
                        newTransactionsData.add(transactionInfo);
                    }
                }
                transactionsData.clear();
                transactionsData.addAll(newTransactionsData);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        return rootView;
    }
}
