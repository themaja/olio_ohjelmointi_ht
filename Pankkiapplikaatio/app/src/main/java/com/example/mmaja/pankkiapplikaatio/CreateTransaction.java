package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class CreateTransaction extends AppCompatActivity {

    private String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_transaction);
        userid = getIntent().getStringExtra("userid");

    }

    public void createDeposit (View v) {
        Intent intent = new Intent(this, CreateDeposit.class);
        intent.putExtra("userid", userid);
        startActivity(intent);
        finish();
    }

    public void createWithdraw(View v) {
        Intent intent = new Intent(this, CreateWithdraw.class);
        intent.putExtra("userid", userid);
        startActivity(intent);
        finish();
    }

    public void createTransfer( View v) {
        Intent intent = new Intent(this, CreateTransfer.class);
        intent.putExtra("userid", userid);
        startActivity(intent);
        finish();
    }

    public void createCardPayment (View v) {
        Intent intent = new Intent(this, CreateCardPayment.class);
        intent.putExtra("userid", userid);
        startActivity(intent);
        finish();
    }

    public void cancelTransaction (View v) {
        finish();
    }


}
