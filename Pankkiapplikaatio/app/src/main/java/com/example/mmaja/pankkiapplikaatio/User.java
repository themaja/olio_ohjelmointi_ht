package com.example.mmaja.pankkiapplikaatio;

import java.util.ArrayList;

public class User implements java.io.Serializable {
    protected String userID = "";
    protected String password = "";
    private String name = "";
    private String ssn = "";
    private String phone = "";
    private String email = "";
    private String address = "";
    private ArrayList<Account> accountList;

    public User (String id, String pw, String name, String ssn) {
        this.userID = id;
        this.password = pw;
        this.name = name;
        this.ssn = ssn;
        this.accountList = new ArrayList();
    }

    public void addAccount(int idCounter, double b, double i, boolean s) {
        accountList.add(new Account(idCounter, b,i,s));
    }

    public String getUserID() {
        return this.userID;
    }

    public String getPassword() {
        return this.password;
    }

    public String getName() {
        return this.name;
    }

    public String getSsn() {
        return this.ssn;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getEmail() {
        return this.email;
    }

    public String getAddress() {
        return this.address;
    }

    public ArrayList<Account> getAccountList() {
        return this.accountList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}

//class Admin extends User {
//    private static Admin admin = new Admin();
//    public Admin() {
//        super ("admin","admin","admin","admin");
//    }
//
//    public static Admin getInstance() {
//        return admin;
//    }
//}