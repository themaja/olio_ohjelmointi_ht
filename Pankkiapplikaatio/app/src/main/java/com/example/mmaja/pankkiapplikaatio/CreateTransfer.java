package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CreateTransfer extends AppCompatActivity {

    private Spinner sAccounts;
    private EditText etAccount;
    private EditText etAmount;
    private TextView tvNotification;

    private Bank bank;
    private String userid;
    private User user;
    private Account account;
    private FileIO io;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_transfer);
        etAccount = findViewById(R.id.etAccount);
        etAmount = findViewById(R.id.etLimit);
        tvNotification = findViewById(R.id.twNotification);

        io = new FileIO();
        bank = io.loadBank(this);
        userid = getIntent().getStringExtra("userid");
        user = bank.getUserByID(userid);

        // Populate the spinner with currentAccounts
        List<String> spinnerArray =  new ArrayList<String>();
        for (Account a : user.getAccountList()) {
            if (a.getSavingsAccount() == false) {
                String accountInfo = String.format("%s\t\t%.2f €", a.getAccountID(), a.getBalance());
                spinnerArray.add(accountInfo);
            }
        }

        // Set up the spinner
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sAccounts = findViewById(R.id.spinner);
        sAccounts.setAdapter(arrayAdapter);
    }

    public void createTransfer(View v) {
        if (etAccount.getText().toString().isEmpty()) {
            tvNotification.setText("Syötä saajan tilinumero!");
        } else if (etAmount.getText().toString().isEmpty()){
            tvNotification.setText("Syötä summa!");
        } else {
            int index = sAccounts.getSelectedItemPosition();
            int i = 0;

            // Find the correct currentAccount by spinners selections index
            for (Account a : user.getAccountList()) {
                if (a.getSavingsAccount() == false) {
                    if (index == i) {
                        account = a;
                        break;
                    } else {
                        i++;
                    }
                }
            }

            if (account == null) {
                tvNotification.setText("Tiliä ei ole valittu!");
            } else {
                String fromAccount = account.getAccountID();
                String toAccount = etAccount.getText().toString();
                double amount = Double.parseDouble(etAmount.getText().toString());

                // check if account has enough money to make transfer
                if (account.takeMoney(amount)) {
                    bank.addTransaction(this, amount, fromAccount, toAccount, Type.transfer);

                    //check if the receiver account is in the same bank and if so, add money
                    for (User u : bank.getUserList()) {
                        for (Account a: u.getAccountList()) {
                            if (a.getAccountID().equals(toAccount)) {
                                a.addMoney(amount);
                                break;
                            }
                        }
                    }
                    io.saveBank(bank, this);

                    Intent intent = new Intent(this, MainMenu.class);
                    intent.putExtra("userid", userid);
                    startActivity(intent);
                    finish();
                } else {
                    tvNotification.setText("Tilin saldo ei riitä!");
                }
            }
        }
    }

    public void cancel(View v) {
        finish();
    }
}
