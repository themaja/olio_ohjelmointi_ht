package com.example.mmaja.pankkiapplikaatio;

import android.content.Context;

import java.util.ArrayList;

public class Bank implements java.io.Serializable {
    private String name = "Paraspankki";
    private ArrayList<User> userList;
    private ArrayList<Transaction> transactionList;
    private static Bank instance;
    private FileIO io = new FileIO();
    private int accountIdCounter;
    private int bankCardIdCounter;


    private Bank() {
        this.userList = new ArrayList();
        this.transactionList = new ArrayList();
        this.userList.add(new User("admin", "admin", "admin", "admin"));
        this.accountIdCounter = 0;
        this.bankCardIdCounter = 99999999;
    }

    public static Bank getInstance() {
        if (instance == null) {
            instance = new Bank();
        }
        return instance;
    }

    public int getAccountIdCounter() {
        this.accountIdCounter++;
        return this.accountIdCounter;
    }

    public int getBankCardIdCounter() {
        this.bankCardIdCounter--;
        return this.bankCardIdCounter;
    }


    public void addUser (String id, String pw, String name, String ssn) {
        User u = new User(id, pw, name, ssn);
        this.userList.add(u);
    }

    public void addTransaction (Context context, double amount, String from, String to, Type type) {
        Transaction t = new Transaction(amount, from, to, type);
        this.transactionList.add(t);
        io.saveTransactionToXML(context, t);

    }

    public String getName() {
        return this.name;
    }

    public ArrayList<User> getUserList() {
        return this.userList;
    }

    public User getUserByID(String ID) {
        User user = null;
        for (User u : this.getUserList()) {
            if (u.getUserID().equals(ID)) {
                user = u;
                return user;
            }
        }
        return user;
    }

    public ArrayList<Transaction> getTransactionList() {
        return this.transactionList;
    }
}