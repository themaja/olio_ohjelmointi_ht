package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

public class CreateAccount extends AppCompatActivity {

    private EditText deposit;
    private Switch accountTypeSwitch;

    private Bank bank;
    private User user;
    private String userid;
    private FileIO io;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        deposit = findViewById(R.id.etDeposit);
        accountTypeSwitch = findViewById(R.id.switch1);

        io = new FileIO();
        bank = io.loadBank(this);
        userid = getIntent().getStringExtra("userid");

        for (User u : bank.getUserList()) {
            if (u.getUserID().equals(userid)) {
                user = u;
                break;
            }
        }




    }

    // These methods are associated with the buttons.
    public void createAccount(View V) {
        double dep;
        if (deposit.getText().toString().isEmpty()) {
            dep = 0;
        } else {
            dep = Double.parseDouble(deposit.getText().toString());
        }
        if (accountTypeSwitch.isChecked()) {
            user.addAccount(bank.getAccountIdCounter(), dep, 0.02, true);
        } else {
            user.addAccount(bank.getAccountIdCounter(), dep,0,false);
        }
        io.saveBank(bank, this);
        Intent intent = new Intent(CreateAccount.this, MainMenu.class);
        intent.putExtra("userid", userid);
        startActivity(intent);
        finish();
    }

    public void cancelAccountCreation(View v) {
        finish();
    }


}
