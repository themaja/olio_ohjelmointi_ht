package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class UserSettings extends AppCompatActivity {

    private EditText etName;
    private EditText etPhone;
    private EditText etEmail;
    private EditText etAddress;

    private Bank bank;
    private String userid;
    private User user;
    private FileIO io;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        etName = findViewById(R.id.etName);
        etPhone = findViewById(R.id.etPhone);
        etEmail = findViewById(R.id.etEmail);
        etAddress = findViewById(R.id.etAddress);

        io = new FileIO();
        bank = io.loadBank(this);
        userid = getIntent().getStringExtra("userid");
        user = bank.getUserByID(userid);

        etName.setText(user.getName());
        etPhone.setText(user.getPhone());
        etEmail.setText(user.getEmail());
        etAddress.setText(user.getAddress());

    }

    public void save (View v) {
        user.setName(etName.getText().toString());
        user.setPhone(etPhone.getText().toString());
        user.setEmail(etEmail.getText().toString());
        user.setAddress(etAddress.getText().toString());

        io.saveBank(bank,this);
        Intent intent = new Intent(this, MainMenu.class);
        intent.putExtra("userid", userid);
        startActivity(intent);
        finish();
    }

    public void cancel (View v) {
        finish();
    }
}
