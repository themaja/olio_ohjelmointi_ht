package com.example.mmaja.pankkiapplikaatio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class CreateUser extends AppCompatActivity {

    private EditText userID;
    private EditText password;
    private EditText password2;
    private EditText name;
    private EditText ssn;
    private TextView notification;

    private Bank bank;
    private FileIO io;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        userID = findViewById(R.id.etUserID);
        password = findViewById(R.id.etPassword);
        password2 = findViewById(R.id.etPassword2);
        name = findViewById(R.id.etName);
        ssn = findViewById(R.id.etSsn);
        notification = findViewById(R.id.twNotification);

        io = new FileIO();
        bank = io.loadBank(this);
    }


    public void createUser(View v) {
        String inputid = userID.getText().toString();
        String inputpw = password.getText().toString();
        String inputpw2 = password2.getText().toString();
        String inputname = name.getText().toString();
        String inputssn = ssn.getText().toString();

        if (validateNewUser(inputid, inputpw, inputpw2, inputname, inputssn) == true) {
            bank.addUser(inputid,inputpw,inputname,inputssn);
            io.saveBank(bank, this);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void cancelUserCreation(View v) {
        finish();
    }

    public boolean validateNewUser(String inputid, String inputpw, String inputpw2, String inputname, String inputssn) {
        if (inputid.length() > 4) {
            for(User u: bank.getUserList()) {
                if (u.getUserID().equals(inputid)) {
                    notification.setText("Valitsemasi käyttäjätunnus on jo käytössä");
                    return false;
                }
            }
            if ((inputpw.length() > 5) && inputpw.equals(inputpw2)) {
                if (inputname.length() > 0)
                    if (inputssn.length() > 0)
                        return true;
                    else {
                        notification.setText("Syötä kaikki tiedot!");
                        return false;
                    }
                else {
                    notification.setText("Syötä kaikki tiedot!");
                    return false;
                    }
            }
            else {
                notification.setText("Tarkista salasana");
                return false;
            }
        } else {
            notification.setText("Käyttäjätunnus ei kelpaa");
            return false;
        }
    }
}
