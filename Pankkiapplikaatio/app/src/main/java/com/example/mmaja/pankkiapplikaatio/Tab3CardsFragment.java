package com.example.mmaja.pankkiapplikaatio;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Tab3CardsFragment extends Fragment {
    private static final String TAG = "Tab3CardsFragment";

    BankCardRecyclerViewAdapter adapter;

    private Bank bank;
    private String userid;
    private User user;
    private Account account;
    private FileIO io;
    private ArrayList<String> bankCardsData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab3_cards_fragment, container, false);

        io = new FileIO();
        bank = io.loadBank(this.getActivity());
        userid = getArguments().getString("userid");
        user = bank.getUserByID(userid);

        // Populate the spinner with accounts
        List<String> spinnerArray =  new ArrayList<String>();
        for (Account a : user.getAccountList()) {
            String type;
            if (a.getSavingsAccount()) {
                type = "säästötili";
            } else {
                type = "käyttötili";
            }
            String accountInfo = String.format("%s %s %.2f €", a.getAccountID(), type, a.getBalance());
            spinnerArray.add(accountInfo);
        }

        // Set up the spinner
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sBankCards = rootView.findViewById(R.id.spAccounts);
        sBankCards.setAdapter(arrayAdapter);

        // set up the RecyclerView
        RecyclerView recyclerView = rootView.findViewById(R.id.rvBankCards);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BankCardRecyclerViewAdapter(getActivity(), bankCardsData);
        recyclerView.setAdapter(adapter);

        // set up an onItemsSelectedListener for the spinner to change transactions shown
        // in the recyclerview according to the selected account
        sBankCards.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                account = user.getAccountList().get(position);
                String bankCardInfo = "";
                // Populate the RecyclerView with information of each transaction
                ArrayList<String> newBankCardsData = new ArrayList<>();
                for (BankCard b : account.getBankCardList()) {
                    bankCardInfo = String.format("Kortti %s maksuraja %.2f €", b.getCardID(), b.getLimit());
                    newBankCardsData.add(bankCardInfo);
                }
                bankCardsData.clear();
                bankCardsData.addAll(newBankCardsData);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        return rootView;
    }
}
