package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CreateBankCard extends AppCompatActivity {

    private Spinner sAccounts;
    private EditText etLimit;
    private TextView tvNotification;

    private Bank bank;
    private String userid;
    private User user;
    private Account account;
    private FileIO io;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_bank_card);
        etLimit = findViewById(R.id.etLimit);
        tvNotification = findViewById(R.id.twNotification);

        io = new FileIO();
        bank = io.loadBank(this);
        userid = getIntent().getStringExtra("userid");
        user = bank.getUserByID(userid);

        // Populate the spinner with currentAccounts
        List<String> spinnerArray =  new ArrayList<String>();
        for (Account a : user.getAccountList()) {
            if (a.getSavingsAccount() == false) {
                String accountInfo = String.format("käyttötili %s\t%.2f €", a.getAccountID(), a.getBalance());
                spinnerArray.add(accountInfo);
            }
        }

        // Set up the spinner
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sAccounts = findViewById(R.id.spinner);
        sAccounts.setAdapter(arrayAdapter);
    }

    public void createBankCard (View v) {
        int index = sAccounts.getSelectedItemPosition();
        int i = 0;

        // Find the correct currentAccount by spinners selections index
        for (Account a : user.getAccountList()) {
            if (a.getSavingsAccount() == false) {
                if (index == i) {
                    account = a;
                    break;
                } else {
                    i++;
                }
            }
        }

        if (account == null) {
            tvNotification.setText("Tiliä ei ole valittu!");
        } else {
            double limit = 1000;
            if (etLimit.getText().toString().isEmpty()){
                account.addCard(bank.getBankCardIdCounter(), limit);
            } else {
                limit = Double.parseDouble(etLimit.getText().toString());
                account.addCard(bank.getBankCardIdCounter(), limit);
            }
            io.saveBank(bank, this);
            Intent intent = new Intent(this, MainMenu.class);
            intent.putExtra("userid", userid);
            startActivity(intent);
            finish();
        }
    }

    public void cancel (View v) {
        finish();
    }
}
