package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

public class MainMenu extends AppCompatActivity {

    private static final String TAG = "MainMenu";

    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;
    FloatingActionButton fab1;
    FloatingActionButton fab2;
    FloatingActionButton fab3;

    private Bank bank;
    private String userid;
    private FileIO io;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        io = new FileIO();
        bank = io.loadBank(this);
        userid = getIntent().getStringExtra("userid");


        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        // Set up the vViewPager with the sections adapter
        mViewPager = findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tablayout = findViewById(R.id.tabs);
        tablayout.setupWithViewPager(mViewPager);

        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set up the FloatingButtons
        fab1 = findViewById(R.id.fabNewAccount);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenu.this, CreateAccount.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });
        fab2 = findViewById(R.id.fabNewTransaction);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenu.this, CreateTransaction.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });
        fab2.hide();
        fab3 = findViewById(R.id.fabNewCard);
        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenu.this, CreateBankCard.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });
        fab3.hide();

        // set up a pageChangeListener to show the correct fab
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    fab1.show();
                    fab2.hide();
                    fab3.hide();
                } else if (position == 1) {
                    fab1.hide();
                    fab2.show();
                    fab3.hide();
                } else if (position == 2) {
                    fab1.hide();
                    fab2.hide();
                    fab3.show();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupViewPager (ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("userid", userid);

        Fragment tab1 = new Tab1AccountsFragment();
        Fragment tab2 = new Tab2TransactionsFragment();
        Fragment tab3 = new Tab3CardsFragment();
        tab1.setArguments(bundle);
        tab2.setArguments(bundle);
        tab3.setArguments(bundle);

        adapter.addFragment(tab1, "TILIT");
        adapter.addFragment(tab2, "TILITAPAHTUMAT");
        adapter.addFragment(tab3, "MAKSUKORTIT");

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, UserSettings.class);
            intent.putExtra("userid", userid);
            startActivity(intent);
        }
        if (id == R.id.action_logout) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


}
