package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CreateDeposit extends AppCompatActivity {

    private Spinner sAccounts;
    private EditText etAmount;
    private TextView tvNotification;


    private Bank bank;
    private String userid;
    private User user;
    private Account account;
    private FileIO io;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_deposit);
        etAmount = findViewById(R.id.etLimit);
        tvNotification = findViewById(R.id.twNotification);

        io = new FileIO();
        bank = io.loadBank(this);
        userid = getIntent().getStringExtra("userid");
        user = bank.getUserByID(userid);

        // Populate the spinner with accounts
        List<String> spinnerArray =  new ArrayList<String>();
        for (Account a : user.getAccountList()) {
            String type;
            if (a.getSavingsAccount()) {
                type = "säästötili";
            } else {
                type = "käyttötili";
            }
            String accountInfo = String.format("%s %s %.2f €", a.getAccountID(), type, a.getBalance());
            spinnerArray.add(accountInfo);
        }

        // Set up the spinner
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sAccounts = findViewById(R.id.spinner);
        sAccounts.setAdapter(arrayAdapter);
    }

    public void createDeposit (View v) {
        if (etAmount.getText().toString().isEmpty()){
            tvNotification.setText("Syötä summa!");
        } else {
            account = user.getAccountList().get(sAccounts.getSelectedItemPosition());

            if (account == null) {
                tvNotification.setText("Tiliä ei ole valittu!");
            } else {
                String toAccount = account.getAccountID();
                double amount = Double.parseDouble(etAmount.getText().toString());

                account.addMoney(amount);
                bank.addTransaction(this, amount,"", toAccount, Type.deposit);
                io.saveBank(bank, this);

                Intent intent = new Intent(this, MainMenu.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
                finish();
            }
        }
    }

    public void cancel (View v) {
        finish();
    }
}
