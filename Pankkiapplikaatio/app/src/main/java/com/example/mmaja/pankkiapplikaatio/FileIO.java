package com.example.mmaja.pankkiapplikaatio;

import android.content.Context;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class FileIO implements java.io.Serializable {

    private static int transactionId = 1;

    public FileIO() {
    }

    // Saves bank information to file
    public void saveBank (Bank bank, Context context) {
        try {
            FileOutputStream fos = context.openFileOutput("bankobjects.ser", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(bank);
            oos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Loads bank information from file
    public Bank loadBank (Context context) {
        Bank bank;
        try {

            FileInputStream fis = context.openFileInput("bankobjects.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            bank = (Bank) ois.readObject();
            ois.close();
            fis.close();
            return bank;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } return null;
    }

    // Saves transaction to XML file
    public void saveTransactionToXML (Context context, Transaction transaction) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        Document document = null;
        Node root = null;

        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        // Check if it's the first transaction ever made
        if (transactionId == 1) {
            document = db.newDocument();
            root = document.createElement("Transactions");
            document.appendChild(root);
        } else {
            try {
                FileInputStream fis = context.openFileInput("transactions.xml");
                document = db.parse(fis);
                NodeList rootList = document.getElementsByTagName("Transactions");
                root = rootList.item(0);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }

        }

        // Create a new transaction node to the document

        Element element = document.createElement("Transaction");
        root.appendChild(element);

        Attr attr = document.createAttribute("Id");
        attr.setValue(Integer.toString(transactionId));
        transactionId++;
        element.setAttributeNode(attr);

        Element amount = document.createElement("Amount");
        amount.appendChild(document.createTextNode(String.format("%.2f €", transaction.getAmount())));
        element.appendChild(amount);

        Element sourceAccount = document.createElement("Source_account");
        sourceAccount.appendChild(document.createTextNode(transaction.getFromAccountID()));
        element.appendChild(sourceAccount);

        Element targetAccount = document.createElement("Target_account");
        targetAccount.appendChild(document.createTextNode(transaction.getToAccountID()));
        element.appendChild(targetAccount);

        Element type = document.createElement("Type");
        type.appendChild(document.createTextNode(transaction.getType().toString()));
        element.appendChild(type);

        Element date = document.createElement("Date");
        date.appendChild(document.createTextNode(df.format(transaction.getDate())));
        element.appendChild(date);

        // Save the XML file
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            DOMSource source = new DOMSource(document);
            FileOutputStream fos = context.openFileOutput("transactions.xml", Context.MODE_PRIVATE);
            StreamResult result = new StreamResult(fos);
            transformer.transform(source, result);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
