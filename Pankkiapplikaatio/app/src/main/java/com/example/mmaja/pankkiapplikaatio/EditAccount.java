package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;

public class EditAccount extends AppCompatActivity {

    private Switch accountTypeSwitch;

    private Bank bank;
    private String userid;
    private User user;
    private Account account;
    private int accIndex;
    private FileIO io;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        accountTypeSwitch = findViewById(R.id.switch1);

        userid = getIntent().getStringExtra("userid");
        accIndex = getIntent().getIntExtra("accIndex", 0);
        io = new FileIO();
        bank = io.loadBank(this);
        user = bank.getUserByID(userid);
        account = user.getAccountList().get(accIndex);

        if (account.getSavingsAccount()) {
            accountTypeSwitch.setChecked(true);
        }
    }


    // These methods are associated with the buttons.
    public void editAccount(View V) {
        if (accountTypeSwitch.isChecked()) {
            account.setSavingsAccount(true);
            account.setInterest(0.02);
        } else {
            account.setSavingsAccount(false);
            account.setInterest(0);
        }
        io.saveBank(bank, this);
        Intent intent = new Intent(EditAccount.this, MainMenu.class);
        intent.putExtra("userid", userid);
        startActivity(intent);
        finish();
    }

    public void cancelEdit (View v) {
        finish();
    }
}
