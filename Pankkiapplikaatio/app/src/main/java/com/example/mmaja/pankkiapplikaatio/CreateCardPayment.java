package com.example.mmaja.pankkiapplikaatio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CreateCardPayment extends AppCompatActivity {

    private Spinner sCards;
    private EditText etAccount;
    private EditText etAmount;
    private TextView tvNotification;

    private Bank bank;
    private String userid;
    private User user;
    private Account account;
    private BankCard card;
    private FileIO io;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_card_payment);
        etAccount = findViewById(R.id.etAccount);
        etAmount = findViewById(R.id.etLimit);
        tvNotification = findViewById(R.id.tvNotification);

        io = new FileIO();
        bank = io.loadBank(this);
        userid = getIntent().getStringExtra("userid");
        user = bank.getUserByID(userid);

        // Populate the spinner with bankCards
        List<String> spinnerArray =  new ArrayList<String>();
        for (Account a : user.getAccountList()) {
            for (BankCard b : a.getBankCardList()) {
                String cardInfo = String.format("Tilin %s kortti %s\nnostoraja %.2f €", a.getAccountID(), b.getCardID(), b.getLimit());
                spinnerArray.add(cardInfo);
            }

        }

        // Set up the spinner
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sCards = findViewById(R.id.spinner);
        sCards.setAdapter(arrayAdapter);
    }

    public void createCardPayment(View v) {
        if (etAmount.getText().toString().isEmpty()){
            tvNotification.setText("Syötä summa!");
        } else {
            int index = sCards.getSelectedItemPosition();
            int i = 0;

            // Find the correct account and card by spinners selections index
            for (Account a : user.getAccountList()) {
                for (BankCard b : a.getBankCardList()) {
                    if (index == i) {
                        account = a;
                        card = b;
                        break;
                    } else {
                        i++;
                    }
                }
            }

            if (card == null) {
                tvNotification.setText("Korttia ei ole valittu!");
            } else{
                String fromAccount = account.getAccountID();
                double amount = Double.parseDouble(etAmount.getText().toString());

                // check if cards limit allows the payment
                if (card.getLimit() < amount) {
                    tvNotification.setText("Kortin maksuraja ei salli maksua!");
                } else {
                    // check if account has enough money to make payment
                    if (account.takeMoney(amount)) {
                        bank.addTransaction(this, amount, fromAccount, "", Type.cardPayment);
                        io.saveBank(bank, this);

                        Intent intent = new Intent(this, MainMenu.class);
                        intent.putExtra("userid", userid);
                        startActivity(intent);
                        finish();
                    } else {
                        tvNotification.setText("Tilin saldo ei riitä!");
                    }
                }
            }
        }
    }

    public void cancel(View v) {
        finish();
    }
}
