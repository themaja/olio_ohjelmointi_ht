package com.example.mmaja.pankkiapplikaatio;

import java.util.Date;

enum Type implements java.io.Serializable {
    deposit, withdraw, transfer, cardPayment
}

public class Transaction implements java.io.Serializable {
    private double amount = 0;
    private String fromAccountID = "";
    private String toAccountID = "";
    private Type type;
    private Date date;

    public Transaction(double amount, String from, String to, Type t) {
        this.amount = amount;
        this.fromAccountID = from;
        this.toAccountID = to;
        this.type = t;
        this.date = new Date();
    }

    public double getAmount() {
        return amount;
    }

    public String getFromAccountID() {
        return fromAccountID;
    }

    public String getToAccountID() {
        return toAccountID;
    }

    public Type getType() {
        return type;
    }

    public Date getDate() {
        return date;
    }

}
