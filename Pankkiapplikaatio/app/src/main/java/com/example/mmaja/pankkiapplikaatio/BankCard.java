package com.example.mmaja.pankkiapplikaatio;

public class BankCard implements java.io.Serializable {
    private String cardID;
    private double limit;

    public BankCard (int idCounter, double lim) {
        String id = Integer.toString(idCounter);
        this.cardID = ("00000000" + id).substring(id.length());
        this.limit = lim;
    }

    public String getCardID() {
        return this.cardID;
    }

    public double getLimit() {
        return this.limit;
    }
}
