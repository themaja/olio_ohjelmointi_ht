package com.example.mmaja.pankkiapplikaatio;

import java.util.ArrayList;

public class Account implements java.io.Serializable {
    private String accountID;
    private double balance;
    private double interest;
    private boolean savingsAccount;
    private boolean frozen;
    private ArrayList<BankCard> bankCardList;

    public Account(int idCounter, double b, double i, boolean s) {
        String id = Integer.toString(idCounter);
        this.accountID = ("00000000" + id).substring(id.length());
        this.balance = b;
        this.interest = i;
        this.savingsAccount = s;
        this.frozen = false;
        this.bankCardList = new ArrayList();
    }

    public void addMoney (double amount) {
        this.balance = this.balance + amount;
    }

    public boolean takeMoney (double amount) {
        if (this.balance <= amount) {
            return false;
        } else {
            this.balance = this.balance - amount;
            return true;
        }
    }

    public void addCard (int idCounter, double limit) {
        bankCardList.add(new BankCard(idCounter, limit));
    }

    public String getAccountID() {
        return this.accountID;
    }

    public double getBalance() {
        return this.balance;
    }

    public boolean getSavingsAccount() {
        return this.savingsAccount;
    }

    public ArrayList<BankCard> getBankCardList() {
        return bankCardList;
    }

    public void setFrozen(boolean f) {
        this.frozen = f;
    }

    public void setInterest(double i) {
        this.interest = i;
    }

    public void setSavingsAccount(boolean s) {
        this.savingsAccount = s;
    }
}







