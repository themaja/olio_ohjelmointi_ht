
package teht6;

import java.text.DecimalFormat;


abstract class Account {
    protected String id;
    protected double balance;
    DecimalFormat df = new DecimalFormat("#.#");
    
    protected Account (String i, double m) {
        id = i;
        balance = m;    
}
    protected void deposit (double m) {
        this.balance = this.balance + m;        
    }
    
    protected boolean withdraw (double m) {
        this.balance = this.balance - m;
        return true;
    }
    
    protected void printInfo () {
        
    }
    
    protected String getId () {
        return this.id;
    }
    
    protected double getBalance () {
        return this.balance;
    }
}

class NormalAccount extends Account {
    protected NormalAccount (String i, double m) {
        super(i,m);
    }
    
    @Override
    protected boolean withdraw (double m) {
        if (m > this.balance)
            return false;
        else {
            this.balance = this.balance - m;
            return true;
        }
    }
    
    @Override
    protected void printInfo () {
        System.out.format("Tilinumero: %s Tilillä rahaa: %s\n", this.id, df.format(this.balance));
    }
}

class CreditAccount extends Account {
    protected double limit;
    
    protected CreditAccount (String i, double m, double l) {
        super(i,m);
        limit = l;
    }
    
    @Override
    protected boolean withdraw (double m) {
        if (m > this.balance + limit)
            return false;
        else {
            this.balance = this.balance - m;
            return true;
        }
    }
    
    @Override
    protected void printInfo () {
        System.out.format("Tilinumero: %s Tilillä rahaa: %s Luottoraja: %s\n", this.id, df.format(this.balance), df.format(this.limit));
    }
    
    protected double getLimit () {
        return limit;
    }
}