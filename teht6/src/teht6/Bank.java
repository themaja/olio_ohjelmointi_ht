
package teht6;

import java.util.ArrayList;
import java.util.function.Predicate;


public class Bank {
    private ArrayList<Account> accounts = new ArrayList<Account>();
    
    public void addNormalAccount(String id, double money) {
        Account account = new NormalAccount(id, money);
        accounts.add(account);
    }
    
    public void addCreditAccount(String id, double money, double limit) {
        Account account = new CreditAccount(id,money,limit);
        accounts.add(account);
    }    
    public void removeAccount(String id) {
        Predicate<Account> accountPredicate = a -> a.getId().equals(id);
        accounts.removeIf(accountPredicate);
    }
    
    public void deposit (String id, double m) {
        for (Account a:accounts) {
            if (a.getId().equals(id))
                a.deposit(m);
        }
    }
    
    public boolean withdraw (String id, double m) {
        boolean r = false;
        for (Account a:accounts) {
            if (a.getId().equals(id))
                r = a.withdraw(m);
        }
        return r;
    }
    
    public void printAccount (String id) {
        for (Account a:accounts) {
            if (a.getId().equals(id))
                a.printInfo();
        }
    }
    
    public void printAllAccounts () {
        for (Account a:accounts)
            a.printInfo();
    }
}
