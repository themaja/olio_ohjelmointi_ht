
package teht6;

import java.util.Scanner;


public class Mainclass {

    public static void main(String[] args) {
        Bank bank = new Bank();
        Scanner scan = new Scanner(System.in);
        int valinta = 1;
        
        while (valinta != 0) {
        System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
        System.out.println("1) Lisää tavallinen tili");
        System.out.println("2) Lisää luotollinen tili");
        System.out.println("3) Tallenna tilille rahaa");
        System.out.println("4) Nosta tililtä");
        System.out.println("5) Poista tili");
        System.out.println("6) Tulosta tili");
        System.out.println("7) Tulosta kaikki tilit");
        System.out.println("0) Lopeta");
        System.out.print("Valintasi: ");
        valinta = scan.nextInt();
        scan.nextLine();
        
        if (valinta == 1) {
            System.out.print("Syötä tilinumero: ");
            String tn = scan.nextLine();
            System.out.print("Syötä rahamäärä: ");
            double rm = scan.nextDouble();
            bank.addNormalAccount(tn, rm);
            System.out.println("Tili luotu.");
        }
        else if (valinta == 2) {
            System.out.print("Syötä tilinumero: ");
            String tn = scan.nextLine();
            System.out.print("Syötä rahamäärä: ");
            double rm = scan.nextDouble();            
            System.out.print("Syötä luottoraja: ");
            double lr = scan.nextDouble();
            bank.addCreditAccount(tn, rm, lr);
            System.out.println("Tili luotu.");
        }
        else if (valinta == 3) {
            System.out.print("Syötä tilinumero: ");
            String tn = scan.nextLine();
            System.out.print("Syötä rahamäärä: ");
            double rm = scan.nextDouble();
            bank.deposit(tn, rm);
        }
        else if (valinta == 4) {
            System.out.print("Syötä tilinumero: ");
            String tn = scan.nextLine();
            System.out.print("Syötä rahamäärä: ");
            double rm = scan.nextDouble();
            boolean t = bank.withdraw(tn, rm);
            if (t == false)
                System.out.println("Tilin saldo ei riitä.");
        }
        else if (valinta == 5) {
            System.out.print("Syötä poistettava tilinumero: ");
            String tn = scan.nextLine();
            bank.removeAccount(tn);
            System.out.println("Tili poistettu.");
        }
        else if (valinta == 6) {
            System.out.print("Syötä tulostettava tilinumero: ");
            String tn = scan.nextLine();
            bank.printAccount(tn);
        }
        else if (valinta == 7) {
            System.out.println("Kaikki tilit:");
            bank.printAllAccounts();
        }
        else if (valinta == 0) {}
        else
            System.out.println("Valinta ei kelpaa.");
        }
    }
    
}
