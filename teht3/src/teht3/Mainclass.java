/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht3;

/**
 *
 * @author mmaja
 */
import java.util.Scanner;

public class Mainclass {

    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        int valinta = 1;        
        BottleDispenser pullokone = new BottleDispenser();
        
        while (valinta > 0) {
            System.out.println("\n*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            valinta = scan.nextInt();
            if (valinta == 1) {
                pullokone.addMoney();                
            }
            else if (valinta == 2) {
                int index = 1;
                for (Bottle pullo : pullokone.getBottles()) {
                    System.out.println(index + ". Nimi: " + pullo.getName());
                    System.out.println("\tKoko: " + pullo.getSize() +"\tHinta: " + pullo.getPrice());
                    index++;
                }
                System.out.print("Valintasi: ");
                int valinta2 = scan.nextInt();
                pullokone.buyBottle(valinta2-1);
            }
            else if (valinta == 3) {
                pullokone.returnMoney();
            }
            else if (valinta == 4) {
                int index = 1;
                for (Bottle pullo : pullokone.getBottles()) {
                    System.out.println(index + ". Nimi: " + pullo.getName());
                    System.out.println("\tKoko: " + pullo.getSize() +"\tHinta: " + pullo.getPrice());
                    index++;
                }
            }
            else if (valinta == 0) {
                break;
            }
            else
                System.out.println("Virheellinen valinta!");
    }
       }
        
}