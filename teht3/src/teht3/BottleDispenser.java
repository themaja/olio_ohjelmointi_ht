/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht3;

/**
 *
 * @author mmaja
 */
import java.util.ArrayList;

public class BottleDispenser {
    
    private int bottles;
    private double money;
    ArrayList<Bottle> bottle_array = new ArrayList();
    
    public BottleDispenser() {
        bottles = 6;
        money = 0;
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.3, 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.9, 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola Company", 0.3, 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola Company", 0.9, 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Coca-Cola Company", 0.3, 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Coca-Cola Company", 0.9, 0.5, 1.95));
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int i) {
        if (bottles > 0) {
            if (money > bottle_array.get(i).getPrice()){
                money -= bottle_array.get(i).getPrice();
                bottles -= 1;
                System.out.println("KACHUNK! " + bottle_array.get(i).getName() + " tipahti masiinasta!");
                bottle_array.remove(i);
            }
            else
                System.out.println("Syötä rahaa ensin!");
        } else
            System.out.println("Ei pulloja");
    }
    
    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
        money = 0;
    }
    
    public ArrayList<Bottle> getBottles() {
        return bottle_array;
    }


}

